﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DiceMenu : MonoBehaviour
{
	#region Public Config Variables
	[Tooltip("Title display panel reference")]
	public RectTransform Title = null;

	[Tooltip("What icon to use as menu open button.")]
	public string OpenIcon = "\uf151";
	[Tooltip("What icon to use as menu close button.")]
	public string CloseIcon = "\uf150";

	[Tooltip("What icon to use as color selected button.")]
	public string OnIcon = "\uf058";
	[Tooltip("What icon to (not) use for color not selected buttons.")]
	public string OffIcon = " ";
	[Tooltip("What color to use when button is selected")]
	public Color OnColor = Color.yellow;
	[Tooltip("What color to use when button is not selected")]
	public Color OffColor = Color.white;

	[Tooltip("Toggle button reference")]
	public Button ToggleButton;
	[Tooltip("Reference to dice color buttons")]
	public List<DiceButton> DieColorButtons;
	[Tooltip("Reference to dice type buttons")]
	public List<DiceButton> DiceTypeButtons;
	[Tooltip("A collection of control button references")]
	public List<Button> ControlButtons;

	[Tooltip("Default die color.")]
	public DieColor DefaultDieColor = DieColor.Red;
	[Tooltip("Default die type.")]
	public DieType DefaultDieType = DieType.D6;

	[Tooltip("How long it takes to open or close this menu.")]
	[Range(0.1f, 4.2f)]
	public float ToggleDuration = 0.8f;

	[Tooltip("Which animation tween multiplier to use for menu transition")]
	public Easing.Type MenuTransitionType = Easing.Type.EaseInOutCircular;
	#endregion

	#region Private Dataholder Variables
	private bool initialized = true;

	private RectTransform trans;
	private Vector2 panelSize;

	private DieType curSide = DieType.Unknown;
	private DieColor curColor = DieColor.Random;

	private Dictionary<DieColor, TextMeshProUGUI> colorButtonCache;
	private Dictionary<DieType, Button> typeButtonCache;
	private ColorBlock onColors, offColors;

	private float accu = 0.0f;
	private State cur = State.Error;
	private Vector2 sourceSize = Vector2.zero, targetSize = Vector2.zero;
	private TextMeshProUGUI toggleButtonCache;
	private EasingMethod easing;

	private Level levelController;
	#endregion

	#region Getter & Setter
	/// <summary>
	/// Get or set current menu enabed state
	/// </summary>
	public bool Enabled
	{
		get { return cur == State.Opened; }
		set { Toggle(value); }
	}

	/// <summary>
	/// Whether or not current state is interactable
	/// </summary>
	public bool Interactable
	{
		get
		{
			return ((cur == State.Opened) || (cur == State.Closed));
		}
	}

	/// <summary>
	/// Setting the level controller reference
	/// </summary>
	public Level LevelController
	{
		set
		{
			levelController = value;
		}
	}
	#endregion

	#region Constructor
	/// <summary>
	/// On object instantiation, verify
	/// </summary>
	private void Awake()
	{
		// Verify components
		initialized = true;
		if (Title == null)
		{
			Global.ToDebug("[Title] reference is not set.", true);
			initialized = false;
		}
		if ((string.IsNullOrWhiteSpace(OnIcon)) || (OffIcon == null))
		{
			Global.ToDebug("[On Icon] or [Off Icon] for color selection button is not provided.", true);
			initialized = false;
		}
		if ((OnColor == null) || (OffColor == null))
		{
			Global.ToDebug("[On Color] or [Off Color] for type selection button is not provided.", true);
			initialized = false;
		}
		if (ToggleButton == null)
		{
			Global.ToDebug("[Toggle Button] reference is not provided.", true);
			initialized = false;
		}
		if ((DieColorButtons == null) || (DieColorButtons.Count <= 1))
		{
			Global.ToDebug("Insufficient number of [Color Button] assigned to reference", true);
			initialized = false;
		}
		if ((DiceTypeButtons == null) || (DiceTypeButtons.Count <= 1))
		{
			Global.ToDebug("Insufficient number of [Dice Type Button] assigned to reference", true);
			initialized = false;
		}
		if ((ControlButtons == null) || (ControlButtons.Count <= 1))
		{
			Global.ToDebug("[Control Button] references are not provided.", true);
			initialized = false;
		}
		if (ToggleDuration < 0.1)
		{
			Global.ToDebug("Insufficient amount of time allowed for toggling animation.", true);
			initialized = false;
		}
	}

	/// <summary>
	/// Start is called before the first frame update
	/// </summary>
	void Start()
	{
		// If not verified, pause
		if ((!initialized) || (!Initialization()))
			this.enabled = false;
		Global.ToDebug("Initialized, application starts.", false);
	}

	/// <summary>
	/// Initialize current script
	/// </summary>
	/// <returns></returns>
	public bool Initialization()
	{
		initialized = true;

		try
		{
			// Cache size
			trans = this.transform.GetChild(0).GetComponent<RectTransform>();
			Invoke("GetPanelSize", 0.01f);

			// Cache buttons
			colorButtonCache = new Dictionary<DieColor, TextMeshProUGUI>();
			for (int i = 0; i < DieColorButtons.Count; i++)
				colorButtonCache.Add((DieColor)DieColorButtons[i].Flag, DieColorButtons[i].Button.transform.GetChild(0).GetComponent<TextMeshProUGUI>());
			typeButtonCache = new Dictionary<DieType, Button>();
			for (int i = 0; i < DiceTypeButtons.Count; i++)
				typeButtonCache.Add((DieType)DiceTypeButtons[i].Flag, DiceTypeButtons[i].Button);

			// Verify results
			if ((colorButtonCache == null) || (colorButtonCache.Count <= 0))
			{
				Global.ToDebug("Color buttons weren't successfully cached.", true);
				initialized = false;
			}
			if ((typeButtonCache == null) || (typeButtonCache.Count <= 0))
			{
				Global.ToDebug("Type buttons weren't successfully cached.", true);
				initialized = false;
			}
			if (!initialized)
				return false;

			// Cache easing algorithm
			easing = Easing.Get(MenuTransitionType);

			// Finish initialization
			onColors = offColors = DiceTypeButtons[0].Button.colors;
			onColors.normalColor = onColors.selectedColor = OnColor;
			offColors.normalColor = offColors.selectedColor = OffColor;
			// Setup button states
			UpdateColorButtons(curColor = DefaultDieColor);
			UpdateDieTypeButtons(curSide = DefaultDieType);

			// Setup menu state
			toggleButtonCache = ToggleButton.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
			SetState(State.Opened);
			return true;
		}
		catch (Exception error)
		{
			Global.ToDebug("Error occurred while initializing cache.  Error Message: " + error.Message, true);
			return false;
		}
	}
	#endregion

	#region Update
	/// <summary>
	/// Update is called once per frame
	/// </summary>
	void Update()
	{
		// Check for simple case first
		if (Interactable)
			return;

		// Otherwise check for transition
		switch (cur)
		{
			case State.Opening:
			case State.Closing:
				// Check for escape
				if (accu > ToggleDuration)
				{
					// Done, break
					SetState(cur == State.Opening ? State.Opened : State.Closed);
					return;
				}

				// Animation transition
				accu += Time.deltaTime;
				trans.sizeDelta = Vector2.Lerp(sourceSize, targetSize, easing(accu / ToggleDuration));
				break;
		}
	}
	#endregion

	#region Button Event Handler
	/// <summary>
	/// Sets the current die type indicator
	/// </summary>
	/// <param name="side">Number of sides for die type</param>
	public void SetSide(int side)
	{
		UpdateDieTypeButtons((DieType)side);
	}

	/// <summary>
	/// Sets the current color indicator
	/// </summary>
	/// <param name="color">Which die color was selected</param>
	public void SetColor(int color)
	{
		UpdateColorButtons((DieColor)color);
	}

	public void Toggle()
	{
		// Simple verification
		if (!Interactable)
		{
			Global.ToDebug("Current state isn't interactable, please wait until transition ends.", true);
			return;
		}

		// Forward request
		Toggle(cur != State.Opened);
	}

	/// <summary>
	/// Toggle GUI visibility
	/// </summary>
	/// <param name="enabled">Toggle current menu visibility if null, true or false for manu set</param>
	public void Toggle(bool enabled)
	{
		// Simple verification
		if (!Interactable)
		{
			Global.ToDebug("Current state isn't interactable, please wait until transition ends.", true);
			return;
		}

		// Update status
		SetState(enabled ? State.Opening : State.Closing);
	}

	/// <summary>
	/// Add a die to scene using current configuration
	/// </summary>
	public void AddDie()
	{
		// Forward the request if available
		if (levelController != null)
		{
			levelController.AddDie(curColor, curSide);
			return;
		}

		// Display error
		Global.ToDebug("Unable to add a die to the scene, no level is currently being referenced.", true);
	}

	/// <summary>
	/// Roll added dice
	/// </summary>
	public void RollDice()
	{
		// Forward the request if available
		if (levelController != null)
		{
			levelController.RollDice();
			return;
		}

		// Display error
		Global.ToDebug("Unable to roll dice by level, no level is currently being referenced.", true);
	}
	#endregion

	#region State Transition
	/// <summary>
	/// Set current state to indicated
	/// </summary>
	/// <param name="to">Which state to set to</param>
	/// <returns>Whether or not animation setting was completed successfully</returns>
	private bool SetState(State to)
	{
		// Simple verification
		if ((to == State.Error) || (to == cur))
			return false;

		// Determine what to do
		switch (to)
		{
			case State.Opening:
				// Reset animation
				accu = 0.0f;
				sourceSize.x = trans.sizeDelta.x;
				sourceSize.y = trans.sizeDelta.y;
				targetSize.x = trans.sizeDelta.x;
				targetSize.y = panelSize.y;

				// Disable all buttons
				ToggleButtons(false);
				break;
			case State.Opened:
				// Clean up
				toggleButtonCache.text = CloseIcon;

				// Enable all buttons
				ToggleButtons(true);
				break;
			case State.Closing:
				// Reset animation
				accu = 0.0f;
				sourceSize.x = trans.sizeDelta.x;
				sourceSize.y = trans.sizeDelta.y;
				targetSize.x = trans.sizeDelta.x;
				targetSize.y = panelSize.x;

				// Disable all buttons
				ToggleButtons(false);
				break;
			case State.Closed:
				// Clean up
				toggleButtonCache.text = OpenIcon;

				// Disable all buttons, except for toggle
				ToggleButtons(false);
				ToggleButton.interactable = true;
				break;
			case State.Disable:
				this.enabled = false;
				break;
		}

		// State update
		cur = to;
		Global.ToDebug("Current State: " + cur);
		return true;
	}

	/// <summary>
	/// Toggle the button interactable state
	/// </summary>
	/// <param name="setTo">To to enable; false to disable</param>
	private void ToggleButtons(bool setTo)
	{
		ToggleButton.interactable = setTo;
		for (int i = 0; i < DieColorButtons.Count; ++i)
			DieColorButtons[i].Button.interactable = setTo;
		for (int i = 0; i < DiceTypeButtons.Count; ++i)
			DiceTypeButtons[i].Button.interactable = setTo;
		for (int i = 0; i < ControlButtons.Count; ++i)
			ControlButtons[i].interactable = setTo;
	}

	/// <summary>
	/// Update UI color button states and current flag indicator
	/// </summary>
	/// <param name="color">Which color to set active</param>
	private void UpdateColorButtons(DieColor color)
	{
		// Disable current
		colorButtonCache[curColor].text = OffIcon;

		// Update cache
		curColor = color;

		// Enable target
		colorButtonCache[curColor].text = OnIcon;
	}

	/// <summary>
	/// Update UI die type button states and current flag indicator
	/// </summary>
	/// <param name="side">Which side to set active</param>
	private void UpdateDieTypeButtons(DieType side)
	{
		// Disable current
		typeButtonCache[curSide].colors = offColors;

		// Update cache
		curSide = side;

		// Update button selection
		typeButtonCache[curSide].colors = onColors;
		typeButtonCache[curSide].Select();
	}
	#endregion

	#region Helper Methods
	/// <summary>
	/// Delayed panel size get method, to let UI layout group handle resizing first
	/// </summary>
	private void GetPanelSize()
	{
		panelSize = new Vector2(Title.sizeDelta.y, trans.sizeDelta.y);
	}

	/// <summary>
	/// Current menu state
	/// </summary>
	public enum State
	{
		Error = -1,
		Disable,
		Opening,
		Opened,
		Closing,
		Closed
	}
	#endregion
}