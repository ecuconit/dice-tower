﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Level : MonoBehaviour
{
	#region Public Config Variables
	[Tooltip("Main game controller reference.")]
	public GameController MainConroller;

	[Tooltip("Dice spawn location")]
	public Transform SpawnPoint;
	[Tooltip("Where the dice are initially collected before the drop")]
	public Transform Bowl;
	[Tooltip("Camera position before the drop")]
	public Transform DropPoint;
	[Tooltip("Camera position after the drop")]
	public Transform CollectionPoint;

	[Tooltip("A reference to dice menu")]
	public DiceMenu MenuReference;

	[Tooltip("Amount of time it takes for camera transition")]
	[Range(0.1f, 4.2f)]
	public float CameraTransitionDuration = 0.8f;

	[Tooltip("Which animation tween multiplier to use for camera transition")]
	public Easing.Type CameraMovementEasing = Easing.Type.Linear;
	public Easing.Type CameraRotationEasing = Easing.Type.Linear;

	#endregion

	#region Private Dataholder Variables
	protected bool initialized = true;
	protected State cur;
	protected List<Die> added = new List<Die>();

	protected float accu;
	protected Vector3 fromPosition, toPosition;
	protected Quaternion fromRotation, toRotation;

	protected Camera mainCamera;
	protected EasingMethod cameraMovementEasing;
	protected EasingMethod cameraRotationEasing;
	#endregion

	#region Constructor
	/// <summary>
	/// On object construction, verify configs
	/// </summary>
	void Awake()
	{
		// Verify references
		initialized = true;
		if (MainConroller == null)
		{
			Global.ToDebug("Need to reference the [Main Controller].", true);
			initialized = false;
		}
		if (SpawnPoint == null)
		{
			Global.ToDebug("Dice [Spawn Point] not referenced", true);
			initialized = false;
		}
		if (Bowl == null)
		{
			Global.ToDebug("Dice spawn collection [Bowl] not referenced", true);
			initialized = false;
		}
		if (DropPoint == null)
		{
			Global.ToDebug("Camera [Drop Point] transform is not referenced", true);
			initialized = false;
		}
		if (CollectionPoint == null)
		{
			Global.ToDebug("Camera [Collection Point] transform is not referenced", true);
			initialized = false;
		}
		if (MenuReference == null)
		{
			Global.ToDebug("[Menu Reference] referenced not provided", true);
			initialized = false;
		}
		if (CameraTransitionDuration < 0.1f)
		{
			Global.ToDebug("[Camera Transition Duration] is less than expected, need to be at least 0.1f", true);
			initialized = false;
		}

		initialized = AdditionalChecks(initialized);
	}

	/// <summary>
	/// Script start
	/// </summary>
	void Start()
	{
		// Simple verification
		if (!initialized)
			this.enabled = false;

		// Cache easing algorithm
		cameraMovementEasing = Easing.Get(CameraMovementEasing);
		cameraRotationEasing = Easing.Get(CameraRotationEasing);

		// Set menu references
		MenuReference.LevelController = this;

		// Cache main camera
		mainCamera = Camera.main;

		// Component verification
		if (!Initialization())
			this.enabled = false;
	}

	/// <summary>
	/// Any additional checks to be called on awake
	/// </summary>
	/// <param name="initialized">Current initialization success state</param>
	/// <returns>Whether or not additional check was completed successfully.</returns>
	protected abstract bool AdditionalChecks(bool initialized);

	/// <summary>
	/// Individual script initialization
	/// </summary>
	/// <returns>Whether or not script construction was completed successfully</returns>
	protected abstract bool Initialization();
	#endregion

	#region Button Event Handler
	/// <summary>
	/// Add a die to scene using current configuration
	/// </summary>
	/// <param name="color">Color of the die to add</param>
	/// <param name="side">Which type of die to add</param>
	public abstract void AddDie(DieColor color, DieType side);

	/// <summary>
	/// Roll added dice
	/// </summary>
	public abstract void RollDice();
	#endregion

	#region State Change Event
	/// <summary>
	/// State transition interface method
	/// </summary>
	/// <param name="to">Which state to set to</param>
	/// <returns>Whether or not state change can be made successfully</returns>
	public abstract bool SetState(State to);
	#endregion

	#region Helper 
	/// <summary>
	/// Level state progression
	/// </summary>
	public enum State
	{
		Error = -1,
		Initialized,
		Adding,
		Added,
		Dropping,
		Dropped,
		Resetting,
		Resetted
	}
	#endregion
}