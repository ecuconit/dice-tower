﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameController : MonoBehaviour
{
	#region Public Config Variable
	[Tooltip("A list of dice reference prefab assets")]
	public List<DiceSet> Prefabs;
	[Tooltip("A list of dice textures for faces")]
	public List<DiceFaceTexture> DiceFaceTexture;
	[Tooltip("A list of dice textures for colors")]
	public List<DiceColorTexture> DiceColorTexture;
	[Tooltip("How many pixels to same on a texture map.")]
	public int SampleSize = 24;
	#endregion

	#region Private Dataholder Variable
	private bool initialized = true;

	private bool clearDice = false;
	private Dictionary<DieType, GameObject> diceType;
	private Dictionary<DieType, Tuple<Texture2D, Texture2D>> diceFaceTextures;
	private Dictionary<DieColor, Texture2D[]> diceColorTextures;
	private List<Die> spanwedDice = new List<Die>();

private System.Random rand = new System.Random();
	#endregion

	#region Constructor
	/// <summary>
	/// On object instantiation, verify
	/// </summary>
	private void Awake()
	{
		// Verify components
		initialized = true;
		if ((Prefabs == null) || (Prefabs.Count <= 0))
		{
			Global.ToDebug("The main [Game Controller] needs to have a reference to [Prefabs].", true);
			initialized = false;
		}
		if ((DiceFaceTexture == null) || (DiceFaceTexture.Count <= 0))
		{
			Global.ToDebug("The main [Game Controller] needs to have a reference to [Dice Face Textures].", true);
			initialized = false;
		}
		else
		{
			// Check for texture assignment
			if (DiceFaceTexture.Any(x => x.TextureLight == null || x.TextureDark == null))
			{
				Global.ToDebug("Some of the texture in face listing is empty.", true);
				initialized = false;
			}
		}
		if ((DiceColorTexture == null) || (DiceColorTexture.Count <= 0))
		{
			Global.ToDebug("The main [Game Controller] needs to have a reference to [Dice Color Textures].", true);
			initialized = false;
		}
		else
		{
			// Check texture length
			if (DiceColorTexture.Any(x => x.Textures == null || x.Textures.Length <= 0))
			{
				Global.ToDebug("Some of the texture in color listing is empty.", true);
				initialized = false;
			}
		}
	}

	/// <summary>
	/// Start is called before the first frame update
	/// </summary>
	void Start()
	{
		// If not verified, pause
		if ((!initialized) || (!Initialization()))
			this.enabled = false;
		Global.ToDebug("Initialized, application starts.", false);
	}

	/// <summary>
	/// Initialize current script
	/// </summary>
	/// <returns></returns>
	public bool Initialization()
	{
		// Cache scene objects
		diceType = Prefabs.ToDictionary(x => x.Dice, y => y.Prefab);
		diceFaceTextures = DiceFaceTexture.ToDictionary(x => x.Type, x => new Tuple<Texture2D, Texture2D>(x.TextureDark, x.TextureLight));
		diceColorTextures = DiceColorTexture.ToDictionary(x => x.Color, x => x.Textures);

		return true;
	}
	#endregion

	#region Frame Update
	/// <summary>
	/// Update is called once per frame
	/// </summary>
	void Update()
	{

	}

	/// <summary>
	/// Clean up
	/// </summary>
	private void LateUpdate()
	{
		if (clearDice)
		{
			// Kill all cached assets
			for (int i = 0; i < spanwedDice.Count; i++)
			{
				GameObject.Destroy(spanwedDice[i]);
			}
			spanwedDice = new List<Die>();
		}
	}
	#endregion

	#region Public Unitility Methods
	/// <summary>
	/// Creating an instance of a die
	/// </summary>
	/// <param name="type">What type of die to create</param>
	/// <param name="color">What base color to use</param>
	/// <param name="position">Where to spawn the die, if not provided, use origin</param>
	/// <param name="rotation">Initial rotation of the die, if not provided, use random</param>
	/// <returns>Reference to the die script, null otherwise</returns>
	public Die DieSpawn(DieType type, DieColor color, Vector3? position = null, Quaternion? rotation = null)
	{
		// Simple verification
		if ((!diceType.ContainsKey(type)) || (!diceFaceTextures.ContainsKey(type)) || (!diceFaceTextures.ContainsKey(type)))
		{
			Global.ToDebug("Unable to find dice by indicated type or color.", true);
			return null;
		}

		try
		{
			// Spawn new die
			Die die = GameObject.Instantiate(diceType[type], position ?? Vector3.zero, rotation ?? UnityEngine.Random.rotation).GetComponent<Die>();
			die.name = type + " - " + color;
			spanwedDice.Add(die);

			// Swap material on baseline coloring
			Renderer ren = die.GetComponent<Renderer>();
			Texture2D tex = diceColorTextures[color][(int)(Math.Floor(rand.NextDouble() * diceColorTextures[color].Length))];
			ren.material.SetTexture("_MainTex", tex);
			ren.material.SetTexture("_SecondTex", IsLightColor(tex) ? diceFaceTextures[type].Item1 : diceFaceTextures[type].Item2);

			Global.ToDebug("Spawning a new die: " + die.name);
			return die;
		}
		catch (Exception error)
		{
			Global.ToDebug("Unable to spawn a new die successfully.  Error Message: " + error.Message, true);
			return null;
		}
	}
	#endregion

	#region Helper Methods
	/// <summary>
	/// Check to see if the given texture is a light color
	/// </summary>
	/// <param name="tex">Background texture</param>
	/// <returns>Whether or not the texture is light</returns>
	private bool IsLightColor(Texture2D tex)
	{
		// Random sampling
		float total = 0.0f;
		for (int i = 0; i < SampleSize; i++)
		{
			total += tex.GetPixel((int)(rand.NextDouble() * tex.width), (int)(rand.NextDouble() * tex.height)).grayscale;
		}

		return (total / SampleSize) < 0.5f;
	}
	#endregion
}
