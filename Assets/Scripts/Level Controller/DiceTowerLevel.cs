﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceTowerLevel : Level
{
	#region Constructor
	/// <summary>
	/// Any additional checks to be called on awake
	/// </summary>
	/// <param name="initialized">Current initialization success state</param>
	/// <returns>Whether or not additional check was completed successfully.</returns>
	protected override bool AdditionalChecks(bool initialized)
	{
		return initialized;
	}

	/// <summary>
	/// Individual script initialization
	/// </summary>
	/// <returns>Whether or not script construction was completed successfully</returns>
	protected override bool Initialization()
	{
		// Resetting camera
		SetState(State.Resetting);

		return true;
	}
	#endregion

	#region Frame Update
	/// <summary>
	/// Update is called once per frame
	/// </summary>
	void Update()
	{
		// Check for simple case first

		float percent;
		switch (cur)
		{
			case State.Dropping:
			case State.Resetting:
				// Check for escape
				if (accu > CameraTransitionDuration)
				{
					// Done, break
					SetState(cur == State.Dropping ? State.Dropped : State.Resetted);
					return;
				}

				// Animation transition
				accu += Time.deltaTime;
				percent = accu / CameraTransitionDuration;
				mainCamera.transform.position = Vector3.Lerp(fromPosition, toPosition, cameraMovementEasing(percent));
				mainCamera.transform.rotation = Quaternion.Lerp(fromRotation, toRotation, cameraMovementEasing(percent));
				break;
			case State.Adding:
				break;
		}
	}
	#endregion

	#region Inheritance Override
	/// <summary>
	/// Add a die to scene using current configuration
	/// </summary>
	/// <param name="color">Color of the die to add</param>
	/// <param name="side">Which type of die to add</param>
	public override void AddDie(DieColor color, DieType side)
	{
		// Add a die to the bowl
		added.Add(MainConroller.DieSpawn(side, color, SpawnPoint.transform.position));
	}

	/// <summary>
	/// Roll added dice
	/// </summary>
	public override void RollDice()
	{
		// Drop the dice into the tower
		SetState(State.Dropping);
	}

	/// <summary>
	/// State transition interface method
	/// </summary>
	/// <param name="to">Which state to set to</param>
	/// <returns>Whether or not state change can be made successfully</returns>
	public override bool SetState(State to)
	{
		// Simple verification
		if ((to == State.Error) || (to == cur))
			return false;

		switch (to)
		{
			default:
				return false;
			case State.Initialized:
				// Run re-initialization if necessary
				MenuReference.Toggle(true);
				break;
			case State.Adding:
				break;
			case State.Added: break;
			case State.Dropping:
				// Prep camera transition
				accu = 0.0f;
				fromPosition = mainCamera.transform.position;
				fromRotation = mainCamera.transform.rotation;
				toPosition = CollectionPoint.transform.position;
				toRotation = CollectionPoint.transform.rotation;
				MenuReference.Toggle(false);

				// Remove bowl
				Bowl.gameObject.SetActive(false);
				break;
			case State.Dropped: break;
			case State.Resetting:
				// Prep camera transition
				accu = 0.0f;
				fromPosition = mainCamera.transform.position;
				fromRotation = mainCamera.transform.rotation;
				toPosition = DropPoint.transform.position;
				toRotation = DropPoint.transform.rotation;
				MenuReference.Toggle(false);

				// Re-enable bowl
				Bowl.gameObject.SetActive(true);
				break;
			case State.Resetted:
				// Clearn up, move to initialized
				SetState(State.Initialized);
				return true;
		}

		cur = to;
		return true;
	}
	#endregion
}