/**
 * Based on codes from: WyrmTale Games and Game Components (http://www.wyrmtale.com)
 */
using UnityEngine;
using System.Collections;

// Die subclass to expose the D6 side hitVectors
// Note, acute angles, set Ridgitbody -> Collection Detection to Continuous
public class Die_d4 : Die
{

	override protected Vector3 HitVector(int side)
	{
		switch (side)
		{
			case 1: return new Vector3(0.0f, 1.0f, 0.0f);
			case 2: return new Vector3(-0.9f, -0.3f, 0.0f);
			case 3: return new Vector3(0.5f, -0.3f, 0.8f);
			case 4: return new Vector3(0.5f, -0.3f, -0.8f);
		}
		return Vector3.zero;
	}
}