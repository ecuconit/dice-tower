﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieDetect : MonoBehaviour
{
	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}

	#region Helper 
	/// <summary>
	/// Using raycast hit to detect which face is up
	/// </summary>
	/// <remarks>Reference: https://answers.unity.com/questions/128747/detect-face-of-cube-clicked.html</remarks>
	/// <param name="hit">Raycast hit target</param>
	/// <returns>Which face is up</returns>
	public MCFace GetHitFace(RaycastHit hit)
	{
		Vector3 incomingVec = hit.normal - Vector3.up;

		if (incomingVec == new Vector3(0, -1, -1))
			return MCFace.South;

		if (incomingVec == new Vector3(0, -1, 1))
			return MCFace.North;

		if (incomingVec == new Vector3(0, 0, 0))
			return MCFace.Up;

		if (incomingVec == new Vector3(1, 1, 1))
			return MCFace.Down;

		if (incomingVec == new Vector3(-1, -1, 0))
			return MCFace.West;

		if (incomingVec == new Vector3(1, -1, 0))
			return MCFace.East;

		return MCFace.None;
	}

	/// <summary>
	/// Die face
	/// </summary>
	public enum MCFace
	{
		None,
		Up,
		Down,
		East,
		West,
		North,
		South
	}
	#endregion
}