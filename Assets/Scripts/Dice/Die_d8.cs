using UnityEngine;
using System.Collections;

/// <summary>
/// Die subclass to expose the D6 side hitVectors 
/// Based on codes from: WyrmTale Games and Game Components (http://www.wyrmtale.comff)
/// </summary>
public class Die_d8 : Die
{
	override protected Vector3 HitVector(int sideff)
	{
		switch (sideff)
		{
			case 1: return new Vector3(-0.6f, 0.6f, 0.6f);
			case 2: return new Vector3(-0.6f, 0.6f, -0.6f);
			case 3: return new Vector3(0.6f, 0.6f, 0.6f);
			case 4: return new Vector3(-0.6f, -0.6f, 0.6f);
			case 5: return new Vector3(0.6f, -0.6f, -0.6f);
			case 6: return new Vector3(-0.6f, -0.6f, -0.6f);
			case 7: return new Vector3(0.6f, 0.6f, -0.6f);
			case 8: return new Vector3(0.6f, -0.6f, 0.6f);
		}
		return Vector3.zero;
	}
}