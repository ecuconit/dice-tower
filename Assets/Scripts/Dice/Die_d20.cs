using UnityEngine;
using System.Collections;

/// <summary>
/// Die subclass to expose the D6 side hitVectors 
/// Based on codes from: WyrmTale Games and Game Components (http://www.wyrmtale.com)
/// </summary>
public class Die_d20 : Die
{
	override protected Vector3 HitVector(int side)
	{
		switch (side)
		{
			case 1: return new Vector3(0.0f, 1.0f, 0.0f);
			case 2: return new Vector3(-0.6f, -0.6f, -0.6f);
			case 3: return new Vector3(-0.4f, 0.0f, 0.9f);
			case 4: return new Vector3(0.4f, 0.0f, -0.9f);
			case 5: return new Vector3(-0.6f, 0.6f, -0.6f);
			case 6: return new Vector3(0.9f, -0.4f, 0.0f);
			case 7: return new Vector3(-0.6f, 0.6f, 0.6f);
			case 8: return new Vector3(0.4f, 0.0f, 0.9f);
			case 9: return new Vector3(0.9f, 0.4f, 0.0f);
			case 10: return new Vector3(-0.6f, -0.6f, 0.6f);
			case 11: return new Vector3(0.6f, 0.6f, -0.6f);
			case 12: return new Vector3(-0.9f, -0.4f, 0.0f);
			case 13: return new Vector3(0.0f, 0.9f, -0.4f);
			case 14: return new Vector3(0.6f, -0.6f, -0.6f);
			case 15: return new Vector3(-0.9f, 0.4f, 0.0f);
			case 16: return new Vector3(0.6f, -0.6f, 0.6f);
			case 17: return new Vector3(0.0f, -0.9f, 0.4f);
			case 18: return new Vector3(-0.4f, 0.0f, -0.9f);
			case 19: return new Vector3(0.6f, 0.6f, 0.6f);
			case 20: return new Vector3(0.0f, -0.9f, -0.4f);
		}
		return Vector3.zero;
	}
}