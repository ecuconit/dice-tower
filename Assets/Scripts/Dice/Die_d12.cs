using UnityEngine;
using System.Collections;

/// <summary>
/// Die subclass to expose the D6 side hitVectors 
/// Based on codes from: WyrmTale Games and Game Components (http://www.wyrmtale.comf)
/// </summary>
public class Die_d12 : Die
{
	override protected Vector3 HitVector(int sidef)
	{
		switch (sidef)
		{
			case 1: return new Vector3(0.5f, 0.0f, 0.9f);
			case 2: return new Vector3(-0.5f, 0.0f, 0.9f);
			case 3: return new Vector3(-0.5f, 0.0f, -0.9f);
			case 4: return new Vector3(0.0f, 0.9f, -0.5f);
			case 5: return new Vector3(-0.9f, -0.5f, 0.0f);
			case 6: return new Vector3(-0.9f, 0.5f, 0.0f);
			case 7: return new Vector3(0.9f, 0.5f, 0.0f);
			case 8: return new Vector3(0.9f, -0.5f, 0.0f);
			case 9: return new Vector3(0.5f, 0.0f, -0.9f);
			case 10: return new Vector3(0.0f, 0.9f, 0.5f);
			case 11: return new Vector3(0.0f, -0.9f, -0.5f);
			case 12: return new Vector3(0.0f, -0.9f, 0.5f);
		}
		return Vector3.zero;
	}
}