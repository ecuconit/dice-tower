﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Dice set configuration
/// </summary>
[Serializable]
public class DiceSet
{
	public DieType Dice;
	public GameObject Prefab;
}

/// <summary>
/// Dice material for faces
/// </summary>
[Serializable]
public class DiceFaceTexture
{
	public DieType Type;
	public Texture2D TextureDark;
	public Texture2D TextureLight;
}

/// <summary>
/// Dice material for class
/// </summary>
[Serializable]
public class DiceColorTexture
{
	public DieColor Color;
	public Texture2D[] Textures;
}

/// <summary>
/// Dice button set
/// </summary>
[Serializable]
public class DiceButton
{
	public int Flag;
	public Button Button;
}