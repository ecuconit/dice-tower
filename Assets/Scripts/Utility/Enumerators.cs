﻿#region Dice Identity Enumerator
/// <summary>
/// Dice type identifier
/// </summary>
public enum DieType
{
	Unknown = -1,
	D4 = 4,
	D6 = 6,
	D8 = 8,
	D10 = 10,
	D12 = 12,
	D20 = 20
}

/// <summary>
/// Basic color group identifier
/// </summary>
public enum DieColor
{
	Random = -1,
	Red = 0,
	Orange = 1,
	Yellow = 2,
	Green = 3,
	Blue = 4,
	Indigo = 5,
	Purple = 6,
	White = 7,
	Black = 8
}
#endregion