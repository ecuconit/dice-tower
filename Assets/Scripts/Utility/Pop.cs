﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pop : MonoBehaviour
{
	#region Public Config Variables
	public float force = 20.0f;
	public float spin = 2.0f;
	#endregion

	#region Private Dataholder Variables
	private Rigidbody rig;
	#endregion

	#region Constructor
	/// <summary>
	/// On start, cache rigid body for later use
	/// </summary>
	private void Awake()
	{
		rig = GetComponent<Rigidbody>();
	}
	#endregion

	#region Event Handler
	/// <summary>
	/// On mouse click, bounce
	/// </summary>
	private void OnMouseUpAsButton()
	{
		rig.AddForce(Vector3.up * force, ForceMode.Impulse);
		rig.AddRelativeTorque(new Vector3(Random.value * 2.0f - 1.0f, Random.value * 2.0f - 1.0f, Random.value * 2.0f - 1.0f) * spin, ForceMode.Impulse);
	}
	#endregion
}