﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// An easing algorithm factory, used to get references to easing methods
/// </summary>
/// <see cref="http://gizma.com/easing/"/>
public static class Easing
{
	#region Public Interface Method
	/// <summary>
	/// Get a reference to an easing algorithm
	/// </summary>
	/// <param name="type">Which algorithm to use</param>
	/// <returns>Easing algorithm</returns>
	public static EasingMethod Get(Type type)
	{
		switch (type)
		{
			default: return null;
			case Type.Linear: return Linear;

			case Type.EaseInQuad: return EaseInQuad;
			case Type.EaseOutQuad: return EaseOutQuad;

			case Type.EaseInOutQuad: return EaseInOutQuad;
			case Type.EaseInCubic: return EaseInCubic;
			case Type.EaseInOutCubic: return EaseInOutCubic;

			case Type.EaseInQuart: return EaseInQuart;
			case Type.EaseOutQuart: return EaseOutQuart;
			case Type.EaseInOutQuart: return EaseInOutQuart;

			case Type.EaseInQuint: return EaseInQuint;
			case Type.EaseOutQuint: return EaseOutQuint;
			case Type.EaseInOutQuint: return EaseInOutQuint;

			case Type.EaseInSine: return EaseInSine;
			case Type.EaseOutSine: return EaseOutSine;
			case Type.EaseInOutSine: return EaseInOutSine;

			case Type.EaseInCicular: return EaseInCicular;
			case Type.EaseOutCircular: return EaseOutCircular;
			case Type.EaseInOutCircular: return EaseInOutCircular;

			case Type.EaseInExponential: return EaseInExponential;
			case Type.EaseOutExponential: return EaseOutExponential;
			case Type.EaseInOutExponential: return EaseInOutExponential;

			case Type.EaseInBack: return EaseInBack;
			case Type.EaseOutBack: return EaseOutBack;
			case Type.EaseInOutBack: return EaseInOutBack;
		}
	}
	#endregion

	#region Easing Algorithms
	/// <summary>
	/// No easing
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float Linear(float t) { return t; }

	/// <summary>
	/// Accelerating from zero velocity
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseInQuad(float t) { return t * t; }
	/// <summary>
	/// Decelerating to zero velocity
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseOutQuad(float t) { return t * (2.0f - t); }
	/// <summary>
	/// Acceleration until halfway, then deceleration
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseInOutQuad(float t) { return t < .5 ? 2.0f * t * t : -1.0f + (4.0f - 2.0f * t) * t; }

	/// <summary>
	/// Accelerating from zero velocity 
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseInCubic(float t) { return t * t * t; }
	/// <summary>
	/// Acceleration until halfway, then deceleration 
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseInOutCubic(float t) { return t < .5 ? 4.0f * t * t * t : (t - 1.0f) * (2.0f * t - 2.0f) * (2.0f * t - 2.0f) + 1.0f; }

	/// <summary>
	/// Accelerating from zero velocity
	///     f(x) = x ^ 4
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseInQuart(float t) { return t * t * t * t; }
	/// <summary>
	/// Decelerating to zero velocity 
	///     f(x) = 1 - (x - 1)^r
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseOutQuart(float t) { return 1 - (--t) * t * t * t; }
	/// <summary>
	/// Acceleration until halfway, then deceleration
	///     f(x) = (1/2)((2x)^4)        ; [0, 0.5)
	///     f(x) = -(1/2)((2x-2)^4 - 2) ; [0.5, 1]
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseInOutQuart(float t) { return t < .5 ? 8.0f * t * t * t * t : 1.0f - 8.0f * (--t) * t * t * t; }

	/// <summary>
	/// Accelerating from zero velocity
	///     f(x) = x ^ 5
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseInQuint(float t) { return t * t * t * t * t; }
	/// <summary>
	/// Decelerating to zero velocity
	///     f(x) = (x - 1)^5 +1
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseOutQuint(float t) { return 1.0f + (--t) * t * t * t * t; }
	/// <summary>
	/// Acceleration until halfway, then deceleration 
	///     y = (1/2)((2x)^5)       ; [0, 0.5)
	///     y = (1/2)((2x-2)^5 + 2) ; [0.5, 1]
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseInOutQuint(float t) { return t < .5 ? 16.0f * t * t * t * t * t : 1.0f + 16.0f * (--t) * t * t * t * t; }

	/// <summary>
	/// Slow start based on sine wave cycle
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseInSine(float t) { return Mathf.Sin((t - 1) * Mathf.PI / 2.0f) + 1.0f; }
	/// <summary>
	/// Slow end based on sine wave cycle
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseOutSine(float t) { return Mathf.Sin(t * Mathf.PI / 2.0f); }
	/// <summary>
	/// Slow start and end, capped at 1, based on sine wave cycle
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseInOutSine(float t) { return 0.5f * (1 - Mathf.Cos(t * Mathf.PI)); }

	/// <summary>
	/// Based on shifted quadrant IV on unit circle
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseInCicular(float t) { return 1.0f - Mathf.Sqrt(1.0f - (t * t)); }
	/// <summary>
	/// Based on shifted quadrant II on unit circle
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseOutCircular(float t) { return Mathf.Sqrt((2.0f - t) * t); }
	/// <summary>
	/// Based on the piecewise circular function
	///     y = (1/2)(1 - Math.Sqrt(1 - 4x^2))           ; [0, 0.5)
	///     y = (1/2)(Math.Sqrt(-(2x - 3)*(2x - 1)) + 1) ; [0.5, 1]
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseInOutCircular(float t) { return t < 0.5f ? 0.5f * (1 - Mathf.Sqrt(1.0f - 4.0f * (t * t))) : 0.5f * (Mathf.Sqrt(-((2.0f * t) - 3.0f) * ((2.0f * t) - 1.0f)) + 1.0f); }

	/// <summary>
	/// Very slow to start, heads off quick. 
	///     f(x) = 2^(10(x-1))
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseInExponential(float t) { return (t == 0.0f) ? t : Mathf.Pow(2.0f, 10.0f * (t - 1.0f)); }
	/// <summary>
	/// Very quick to start, heads off quick. 
	///     f(x) = -2^(-10x) + 1
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseOutExponential(float t) { return (t == 1.0f) ? t : Mathf.Pow(2.0f, 10.0f * t); }
	/// <summary>
	/// Based on the piecewise exponential
	///     f(x) = (1/2)2^(10(2x - 1))         ; [0,0.5)
	///     f(x) = -(1/2)*2^(-10(2x - 1))) + 1 ; [0.5,1]
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseInOutExponential(float t) { if ((t <= 0.0f) || (t >= 1.0f)) return t; return t < 0.5f ? 0.5f * Mathf.Pow(2.0f, (20.0f * t) - 10.0f) : -0.5f * Mathf.Pow(2, (-20.0f * t) + 10.0f) + 1.0f; }

	/// <summary>
	/// Going back, then quickly forward. 
	///     f(x) = x^3 - x*sin(x*pi)
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseInBack(float t) { return t * t * t - t * Mathf.Sin(t * Mathf.PI); }
	/// <summary>
	/// Quick forward then curve back.
	///     y = 1-((1-x)^3-(1-x)*sin((1-x)*pi))
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseOutBack(float t) { float f = (1 - t); return 1 - (f * f * f - f * Mathf.Sin(f * Mathf.PI)); }
	/// <summary>
	/// Based on the piecewise overshooting cubic function. 
	///     f(x) = (1/2)*((2x)^3-(2x)*sin(2*x*pi)); [0, 0.5)
	///     f(x) = (1/2)*(1-((1-x)^3-(1-x)*sin((1-x)*pi))+1); [0.5, 1]
	/// </summary>
	/// <param name="t">Normalized time</param>
	/// <returns>Transformed output</returns>
	public static float EaseInOutBack(float t)
	{
		if (t < 0.5f) { float f = 2.0f * t; return 0.5f * (f * f * f - f * Mathf.Sin(f * Mathf.PI)); }
		else { float f = (1.0f - (2.0f * t - 1)); return 0.5f * (1.0f - (f * f * f - f * Mathf.Sin(f * Mathf.PI))) + 0.5f; }
	}
	#endregion

	#region Easing Type
	/// <summary>
	/// Type of easing algorithm
	/// </summary>
	public enum Type
	{
		Linear = 0,

		EaseInQuad,
		EaseOutQuad,
		EaseInOutQuad,

		EaseInCubic,
		EaseInOutCubic,

		EaseInQuart,
		EaseOutQuart,
		EaseInOutQuart,

		EaseInQuint,
		EaseOutQuint,
		EaseInOutQuint,

		EaseInSine,
		EaseOutSine,
		EaseInOutSine,

		EaseInCicular,
		EaseOutCircular,
		EaseInOutCircular,

		EaseInExponential,
		EaseOutExponential,
		EaseInOutExponential,

		EaseInBack,
		EaseOutBack,
		EaseInOutBack
	}
	#endregion
}

/// <summary>
/// A delegate class type for 
/// </summary>
/// <param name="time">Normalized time value between 0.0 ~ 1.0</param>
/// <returns>Transformed output</returns>
public delegate float EasingMethod(float time);