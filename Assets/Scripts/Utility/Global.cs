﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Global
{
	/// <summary>
	/// Write a message to debug console
	/// </summary>
	/// <param name="message">What message to write</param>
	/// <param name="isError">True if this is an error message, false for success message, default for normal message</param>
	/// <returns>Echo</returns>
	public static string ToDebug(string message, bool? isError = null)
	{
		Debug.Log(string.Format("<color=#{0}>{1}</color>",
			isError == null ? "000000" : isError == true ? "800000ff" : "008080ff",
			message));
		return message;
	}
}