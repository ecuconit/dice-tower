﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eject : MonoBehaviour
{
	#region Public Config Variables
	public float Force = 5.0f;
	public Vector3 Direction = Vector3.forward;
	#endregion

	#region Physics Event Handler
	/// <summary>
	/// On trigger enter, eject object
	/// </summary>
	/// <param name="other">Which object it's collding with</param>
	private void OnTriggerStay(Collider other)
	{
		// Only apply force to a die
		if (other.tag == "Die")
		{
			Global.ToDebug("Adding ejection force to: " + other.name);
			other.GetComponent<Rigidbody>().AddForce(Direction * Force, ForceMode.Force);
		}
	}
	#endregion
}
